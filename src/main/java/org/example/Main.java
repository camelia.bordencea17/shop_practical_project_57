package org.example;

import org.example.config.HibernateConfiguration;
import org.hibernate.SessionFactory;

public class Main {
    public static void main(String[] args) {
        System.out.println("Trying to open SessionFactory");

        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        System.out.println("Session created");
    }
}
